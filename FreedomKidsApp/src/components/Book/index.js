import React, {Component} from 'react';
import {WebView} from "react-native-webview";

// For by him all things were created, in heaven and on earth,
// visible and invisible, whether thrones or dominions or rulers or authorities
// - all things were created through him and for him.
// And he is before all things, and in him all things hold together.
// Colossians 1:16-17

export default class Book extends Component {
    render() {
        const link = this.props.navigation.getParam('link');

        return (
            <WebView source={{uri: link}}/>
        );
    }
}