import React, {Component} from 'react';
import {WebView} from "react-native-webview";

// For even the Son of Man came not to be served,
// But to serve and to give his life as a ransom for many.
// Mark 10:45

export default class Music extends Component {
    render() {
        return (
            <WebView source={{uri: 'http://www.freedomkids.org/music.html'}}/>
        );
    }
}