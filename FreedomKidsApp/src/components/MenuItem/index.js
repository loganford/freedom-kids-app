import React, {Component} from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import {Text} from "native-base";
import {MenuItems} from "./MenuItems";
import Sound from "react-native-sound";

// He himself bore our sins in his body on the tree
// That we might die to sin and live to righteousness
// By his wounds you have been healed
// 1 Peter 2:24

export default class MenuItem extends Component {
    render() {
        var item;
        if (this.props.name === 'Books') {
            item = MenuItems.Books
        } else if (this.props.name === 'Games') {
            item = MenuItems.Games
        } else if (this.props.name === 'Music') {
            item = MenuItems.Music
        }
        console.log(item);
        return (
            <TouchableOpacity onPress={() => {this.props.navigation.navigate(item.name);
                let soundEffect = new Sound(item.sound, Sound.MAIN_BUNDLE, () => soundEffect.play());
            }}>
                <View style={styles.selectionContainer}>
                    <Image source={item.image} style={styles.image}/>
                    <Text style={[styles.text, {color: item.color}]}>{item.text}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}