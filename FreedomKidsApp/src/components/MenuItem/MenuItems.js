import {BLUE, GREEN, MAROON} from "../../assets/styles/common";

export const MenuItems = {
    Books: {
        name: 'Books',
        image: require('../../assets/images/icon-book-no-text.png'),
        color: BLUE,
        text: 'Read'
    },
    Games: {
        name: 'Games',
        image: require('../../assets/images/icon-video-no-text.png'),
        color: GREEN,
        text: 'Play',
        sound: 'TaDa.mp3'
    },
    Music: {
        name: 'Music',
        image: require('../../assets/images/icon-music-no-text.png'),
        color: MAROON,
        text: 'Listen'
    }
};