import {BLUE, FONT_NORMAL, GREEN, MAROON} from "../../assets/styles/common";
import {StyleSheet} from "react-native";

export default StyleSheet.create({
    blue: {color: BLUE},
    green: {color: GREEN},
    maroon: {color: MAROON},
    text: {
        fontSize: 40,
        fontFamily: FONT_NORMAL,
    },
    image: {
        height: 180,
        resizeMode: 'contain'
    },
    selectionContainer: {
        alignItems: 'center',
    },
});