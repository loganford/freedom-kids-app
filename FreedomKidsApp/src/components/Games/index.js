import React, {Component} from 'react';
import {Container, Content, Text} from "native-base";
import styles from './styles';
import {Image, ScrollView, TouchableOpacity} from "react-native";
import {GAMES} from "./Games";
import Sound from "react-native-sound";

// Abide in me, and I in you. As the branch cannot bear fruit by itself,
// unless it abides in the vine, neither can you, unless you abide in me.
// I am the vine; you are the branches. Whoever abides in me and I in him,
// he it is that bears much fruit, for apart from me you can do nothing.
// John 15:4-5

export default class Games extends Component {
    state = {
        games: GAMES
    };
    render() {
        return (
            <Container>
                <Content>
                    <Text style={[styles.text, {fontSize: 40}]}>Games</Text>
                    <Text style={[styles.text, {marginBottom: 15}]}>Select a game to play!</Text>
                    <ScrollView horizontal={true}>
                        {
                            this.state.games.map((item) => (
                                <TouchableOpacity key = {item.id} onPress={() => {
                                    console.log('selected game', item)
                                }}>
                                    <Image key = {item.id} style={styles.image} source={item.image}/>
                                </TouchableOpacity>
                            ))
                        }
                    </ScrollView>
                </Content>
            </Container>
            /*<WebView source={{uri: 'http://www.freedomkids.org/videos.html'}}/>*/
        );
    }
}