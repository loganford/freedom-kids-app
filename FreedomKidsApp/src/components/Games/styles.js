import {StyleSheet} from "react-native";
import {FONT_NORMAL, GREEN} from "../../assets/styles/common";

export default StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: 'contain',
        height: 235,
        width: 300
    },
    text: {
        fontFamily: FONT_NORMAL,
        marginLeft: 15,
        color: GREEN,
        alignSelf: 'center'
    }
});