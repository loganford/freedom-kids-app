import {StyleSheet} from "react-native";

export default StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: 'contain',
        width: 300,
        height: 100,
    },
    view : {
        alignItems: 'center',
        justifyContent: 'center',
        // marginRight: 40
    }
});