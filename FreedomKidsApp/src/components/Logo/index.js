import React, {Component} from 'react';
import {Image, View} from 'react-native';
import styles from './styles';

// Therefore I tell you, do not be anxious about your life
// What you will eat or what you will drink
// Nor about your body, what you will put on
// Is not life more than food and the body more than clothing?
// Matthew 6:25

export default class Logo extends Component {
    render() {
        return (
            <View style={styles.view}>
                <Image source={require('../../assets/images/fk-logo.png')} style={styles.image}/>
            </View>
        );
    }
}