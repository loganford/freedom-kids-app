export const BOOKS = [
    {
        'id': 1,
        'image': require('../../assets/images/bible-basics.png'),
        'link': 'http://online.fliphtml5.com/uoex/yrnp/'
    },{
        'id': 2,
        'image': require('../../assets/images/praise-and-worship.png'),
        'link': 'http://online.fliphtml5.com/uoex/eway/'
    },{
        'id': 3,
        'image': require('../../assets/images/animals.png'),
        'link': 'http://online.fliphtml5.com/uoex/xjgz/'
    },{
        'id': 4,
        'image': require('../../assets/images/nature.png'),
        'link': 'http://online.fliphtml5.com/uoex/zzpg/'
    },{
        'id': 5,
        'image': require('../../assets/images/body-parts.png'),
        'link': 'http://online.fliphtml5.com/uoex/wofz/'
    }
];