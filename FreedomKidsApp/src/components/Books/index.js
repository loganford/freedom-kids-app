import React, {Component} from 'react';
import {Image, ScrollView, TouchableOpacity} from 'react-native';
import { Container, Content, Text} from 'native-base';
import styles from './styles';
import {BLUE} from "../../assets/styles/common";
import {BOOKS} from "./Books";

// Jesus Christ is the same, yesterday, today, and forever
// Hebrews 13:8

export default class Books extends Component {
    state = {
      books: BOOKS
    };
    render() {
        return (
            <Container>
                <Content>
                    <Text style={[styles.text, {fontSize: 40}]}>Books</Text>
                    <Text style={[styles.text, {marginBottom: 15}]}>Scroll to select a book!</Text>
                    <ScrollView horizontal={true}>
                        {
                            this.state.books.map((item) => (
                                <TouchableOpacity key = {item.id} onPress={() => {
                                    this.props.navigation.navigate('Book', {
                                        link: item.link
                                    });
                                }}>
                                    <Image key = {item.id} style={styles.image} source={item.image}/>
                                </TouchableOpacity>
                            ))
                        }
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}