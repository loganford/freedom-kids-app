import {StyleSheet} from "react-native";

export const textStyles = StyleSheet.create({
    header: {
        fontSize: 40,
        padding: 15,
        alignSelf: "center",
        fontFamily: "Avenir"
    },
    body: {
        fontSize: 14,
        padding: 15,
        alignSelf: "center",
        fontFamily: "Avenir"
    }
});