export const BLUE = '#78AAEA';
export const GREEN = '#9FCE63';
export const MAROON = '#80170F';
export const GRAY = '#B0B4B4';
export const WHITE = '#FFFFFF';
export const FONT_NORMAL = 'Chalkboard SE';
export const FONT_BOLD = 'Chalkboard SE-Bold';
