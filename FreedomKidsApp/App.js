/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {StyleSheet} from 'react-native';
import { Container, View, Content, StyleProvider } from 'native-base';
import { createStackNavigator, createAppContainer } from "react-navigation";
import Logo from "./src/components/Logo";
import Books from "./src/components/Books";
import MenuItem from "./src/components/MenuItem"
import {FONT_NORMAL, GRAY, BLUE, GREEN, MAROON} from "./src/assets/styles/common";
import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/material';
import Orientation from "react-native-orientation";
import Music from "./src/components/Music";
import Games from "./src/components/Games";
import Book from "./src/components/Book";

// Now to him who is able to keep you from stumbling
// and to present you blameless before the presence of his glory with great joy,
// to the only God, our Savior, through Jesus Christ our Lord,
// be glory, majesty, dominion, and authority, before all time and now and forever. Amen.
// Jude 1:24-26

class Home extends React.Component {
    static navigationOptions = {
        headerTitle: (
            <Logo/>
        ),
        header: null
    };
    componentDidMount(): void {
        Orientation.lockToLandscape()
    }
    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Content>
                        <Logo/>
                        <View style={styles.menuContainer}>
                            <MenuItem name='Books' navigation = {this.props.navigation}/>
                            <MenuItem name='Games' navigation = {this.props.navigation}/>
                            <MenuItem name='Music' navigation = {this.props.navigation}/>
                        </View>
                    </Content>
                </Container>
            </StyleProvider>
        );
      }
}

const AppNavigator = createStackNavigator({
    Home: Home,
    Books: Books,
    Book: Book,
    Music: Music,
    Games: Games
}, {
    defaultNavigationOptions: {

    }
});

export default createAppContainer(AppNavigator);

export const styles = StyleSheet.create({
    blue: {color: BLUE},
    green: {color: GREEN},
    maroon: {color: MAROON},
    text: {
        fontSize: 40,
        fontFamily: FONT_NORMAL,
    },
    image: {
        height: 180,
        resizeMode: 'contain'
    },
    selectionContainer: {
        alignItems: 'center',
    },
    menuContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    }
});